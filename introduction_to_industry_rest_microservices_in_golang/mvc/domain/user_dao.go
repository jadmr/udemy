package domain

import (
	"fmt"
	"net/http"
	"rest_microservice/mvc/utils"
)

type userDaoInterface interface {
	GetUser(int64) (*User, *utils.ApplicationError)
}

type userDao struct{}

var (
	users = map[int64]*User{
		123: {ID: 123, FirstName: "First", LastName: "Last", Email: "foobar@email.com"},
	}
	UserDao userDaoInterface
)

func init() {
	UserDao = &userDao{}
}

// GetUser returns the user with the matching userID
func (u *userDao) GetUser(userID int64) (*User, *utils.ApplicationError) {
	if user := users[userID]; user != nil {
		return user, nil
	}

	/* NOTE: instructor inserted refactor here to return an ApplicationError pointer
	here instead of an error. Disagree with this because the DAO layer should have no
	knowledge of REST protocol specifics, which it now does by setting a status code here */
	return nil, &utils.ApplicationError{
		Message:    fmt.Sprintf("user %v was not found", userID),
		StatusCode: http.StatusNotFound,
		Code:       "not_found",
	}
}
