package controllers

import (
	"net/http"
	"rest_microservice/mvc/services"
	"rest_microservice/mvc/utils"
	"strconv"

	"github.com/gin-gonic/gin"
)

// GetUser returns the user with the matching id
func GetUser(c *gin.Context) {
	userID, err := strconv.ParseInt(c.Param("user_id"), 10, 64)
	if err != nil {
		apiErr := &utils.ApplicationError{
			Message:    "user_id must be a number",
			StatusCode: http.StatusBadRequest,
			Code:       "bad_request",
		}

		utils.RespondErr(c, apiErr)
		return
	}

	user, apiErr := services.UserService.GetUser(userID)
	if apiErr != nil {
		utils.RespondErr(c, apiErr)
		return
	}

	utils.Respond(c, http.StatusOK, user)
}
