package services

import (
	"rest_microservice/mvc/domain"
	"rest_microservice/mvc/utils"
)

type usersService struct {
}

var (
	UserService usersService
)

// GetUser returns the user from the DAO with the matching user id
func (u *usersService) GetUser(userID int64) (*domain.User, *utils.ApplicationError) {
	return domain.UserDao.GetUser(userID)
}
