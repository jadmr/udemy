import { getNotes } from './notes.js'
import yargs from 'yargs'
import chalk from 'chalk'

// Customize yargs version
yargs.version("1.1.0")

// Create add command
yargs.command({
    command: 'add',
    describe: 'Add a new note',
    handler: () => {
        console.log('Adding a new note!')
    }
})

// Create remove command
yargs.command({
    command: 'remove',
    describe: 'Remove a note',
    handler: () => {
        console.log('Removing an existing note!')
    }
})

/*
Challenge: Add two new commands

1) Setup command to support "list" command (print placeholder message for now)
2) Setup command to support "read" command (print placeholder message for now)
3) Test work
*/

yargs.command({
    command: 'list',
    describe: 'List all notes',
    handler: () => {
        console.log('All notes listed!')
    }
})

yargs.command({
    command: 'read',
    describe: 'Read a note',
    handler: () => {
        console.log('Reading an existing note!')
    }
})

console.log(yargs.argv)