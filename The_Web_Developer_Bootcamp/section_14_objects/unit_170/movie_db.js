var movies = [
    {
        "title": "Amelie",
        "rating": 5,
        "haveWatched": true,
    },
    {
        "title": "The Rise of Skywalker",
        "rating": 2,
        "haveWatched": true,
    },
    {
        "title": "1917",
        "rating": 3,
        "haveWatched": false,
    },
    {
        "title": "Ad Astra",
        "rating": 5,
        "haveWatched": true,
    },
    {
        "title": "Pride & Prejudice",
        "rating": 5,
        "haveWatched": true,
    },
];

movies.forEach(movie => {
    var haveSeen = movie.haveWatched ? "seen" : "not seen";
    console.log(`You have ${haveSeen} '${movie.title}' and rated it with ${movie.rating} stars.`);
});