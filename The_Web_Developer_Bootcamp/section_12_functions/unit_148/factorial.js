function factorial(number) {
    if (number === 0) {
        return 1;
    }
    return number * factorial(number - 1);
}

const readline = require('readline').createInterface({
  input: process.stdin,
  output: process.stdout
});

readline.question("Input a number: ", (value) => {
    var number = parseInt(value, 10);
    readline.close();

    console.log(factorial(number));
});

