function kebabToSnake(kebab) {
    var snake = ""; 

    for (var i = 0; i < kebab.length; i++) {
        var value = kebab[i];

        if (value === '-') {
            snake += '_';
        } else {
            snake += kebab[i];
        }
    }

    return snake;
}

const readline = require('readline').createInterface({
  input: process.stdin,
  output: process.stdout
});

readline.question("Input a string: ", (value) => {
    readline.close();

    console.log(kebabToSnake(value));
});

