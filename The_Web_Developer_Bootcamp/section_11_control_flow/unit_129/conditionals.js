const readline = require('readline').createInterface({
  input: process.stdin,
  output: process.stdout
});

readline.question("How old are you? ", (value) => {
    var age = parseInt(value, 10);
    readline.close();

    if (age < 0) {
        console.log("Can't enter a negative value!");
        return;
    }

    if (age === 21) {
        console.log("Happy 21st birthday!");
    }

    if (age % 2 !== 0) {
        console.log("Your age is odd!");
    }

    // Take the square root and divide the age by it. If no remainder, age is perfect square
    var square = Math.sqrt(age);
    if (age % square === 0) {
        console.log("Your age is a perfect square!")
    }
});

