// Problem 1
console.log("Problem 1");
var count = -10;

while (count <= 19) {
    console.log(count);
    count++;
}

// Problem 2
console.log("\nProblem 2:");
count = 10;

while (count <= 40) {
    console.log(count);
    count += 2;
}

// Problem 3
console.log("\nProblem 3:");
count = 301;

while (count <= 333) {
    console.log(count);
    count += 2;
}

// Problem 4
console.log("\nProblem 4:");
count = 5;

while (count <= 50) {
    if (count % 5 === 0 && count % 3 === 0) {
        console.log(count);
    }
    count++;
}