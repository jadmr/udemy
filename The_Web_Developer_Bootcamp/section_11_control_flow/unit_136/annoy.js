var message = "Are we there yet?";
var answer = prompt(message).toLowerCase();

while (answer !== "yes" && answer != "yeah") {
    answer = prompt(message).toLowerCase();
}

alert("Yay, we finally made it! Refresh to travel again.");