var randomNumber = Math.floor(Math.random() * 10) + 1;

var guess = prompt("Pick a number from 1-10");

while (guess !== randomNumber) {
    var message = guess > randomNumber ? "Too high!" : "Too low!";

    alert("Wrong number! " + message);
    guess = prompt("Pick a number from 1-10");
}

alert("Correct! Refresh to play again.");