var body = document.querySelector("body");
var isBlue = false;

setInterval(function() {
    var color = isBlue ? "blue" : "white";
    body.style.background = color;
    isBlue = !isBlue;
}, 1000)