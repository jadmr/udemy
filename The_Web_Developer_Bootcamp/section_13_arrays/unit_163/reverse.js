function printReverse(list) {
    var temp = []
    for (var i = list.length - 1; i >= 0; i--) {
        temp.push(list[i]);
    }
    console.log(temp);
}

function processList(data) {
    console.log("\n" + data);
    printReverse(data);
}

processList([1, 2, 3, 4, 5, 6]);
processList(["a", "b", "c", "d"]);
processList("hello!");