function maxInList(list) {
    var max = list[0];

    // yeah, yeah, iterating index 0 is redundant...
    list.forEach(function(num) {
        if (num > max) {
            max = num;
        }
    })
    return max;
}

function process(list) {
    console.log(list);
    console.log(maxInList(list));
    console.log();
}

process([-111, -2, -3, -4, -5]);
process([5, 4, 3, 2, 1]);
process([2, 1, -214, 0, 131, 77]);