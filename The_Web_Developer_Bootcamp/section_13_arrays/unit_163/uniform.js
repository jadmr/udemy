function isUniform(list) {
    for(var i = 0; i < list.length; i++) {
        if (i === 0) {
            continue;
        }

        if (list[i-1] !== list[i]) {
            return false;
        }
    }

    return true;
}

function process(list) {
    console.log(list);
    console.log(isUniform(list));
    console.log();
}

process([1]);
process([1, 1, 1, 1, 1]);
process([2, 1, 1, 1, 1]);
process([1, 1, 2, 1, 1]);
process([1, 1, 2, 1, 2]);
process("hello");
process("aaaaa");