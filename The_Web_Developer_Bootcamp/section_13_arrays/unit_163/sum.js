function sum(list) {
    var total = 0;
    list.forEach(function(num) {
        total += num;
    })
    return total;
}

function process(list) {
    console.log(list);
    console.log(sum(list));
    console.log();
}

process([1, 2, 3, 4, 5]);
process([1]);
process([]);